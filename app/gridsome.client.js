import imageUrlBuilder from "@sanity/image-url";

export default function (VueInstance, options) {
  const builder = imageUrlBuilder({
    projectId: options.sanityConfig.projectId,
    dataset: options.sanityConfig.dataset,
  });

  const SanityImageUrl = {
    methods: {
      /**
       * Convert a Sanity image to an image builder (allows better customization)
       * @param {string} source - Image source URL
       */
      urlForImage(source) {
        return builder.image(source);
      },
    },
  };

  VueInstance.mixin(SanityImageUrl);
}
