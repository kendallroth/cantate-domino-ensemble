module.exports = {
  parser: "vue-eslint-parser",
  plugins: ["gridsome"],
  extends: ["plugin:vue/recommended"],
  rules: {
    "gridsome/format-query-block": "off",
    "vue/attribute-hyphenation": "off",
    "vue/html-self-closing": "off",
    "vue/max-attributes-per-line": "off",
    "vue/singleline-html-element-content-newline": "off",
  },
};
