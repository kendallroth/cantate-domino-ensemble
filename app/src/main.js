import "typeface-mulish";
import { format, parseISO } from "date-fns";
import BlockContent from "sanity-blocks-vue-component";
import SvgIcon from "@jamescoyle/vue-icon";

// Components
import Hover from "@components/Hover";
import HeroImage from "@components/Layout/HeroImage";
import Modal from "@components/Layout/Modal";
import PageSection from "@components/Layout/PageSection";
import DefaultLayout from "@layouts/Default";

// Utilities
import { breakpointMixin } from "@utilities/breakpoints";
import { dateFilter } from "@utilities/filters";

import "@styles/styles.scss";

export default (Vue) => {
  Vue.component("BlockContent", BlockContent);
  // Set default layout as a global component
  Vue.component("Layout", DefaultLayout);
  Vue.component("HeroImage", HeroImage);
  Vue.component("Hover", Hover);
  Vue.component("Modal", Modal);
  Vue.component("PageSection", PageSection);
  Vue.component("SvgIcon", SvgIcon);

  Vue.mixin(breakpointMixin);

  Vue.filter("formatDate", dateFilter);

  Vue.prototype.$formatDate = format;
  Vue.prototype.$parseISO = parseISO;
};
