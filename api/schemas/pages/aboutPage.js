export default {
  name: "aboutPage",
  type: "document",
  title: "About Page",
  // NOTE: This is experimental and not recommended! However, the other approach (custom actions)
  //         does not disable adding new documents of this type from the "Add New" menu...
  // NOTE: This appears to no longer work in more recent version of Sanity Studio (disables form)!
  // __experimental_actions: ["update", "publish"],
  fields: [
    {
      name: "groupSummary",
      type: "array",
      title: "Group Summary",
      description: "Summary of the group's history and mission",
      of: [{ type: "block" }],
    },
    {
      name: "conductorName",
      type: "string",
      title: "Conductor Name",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "conductorPhoto",
      type: "image",
      title: "Conductor Photo",
      options: { hotspot: true },
    },
    {
      name: "conductorBio",
      type: "array",
      title: "Conductor Bio",
      description: "Biography of the conductor",
      of: [{ type: "block" }],
    },
    {
      name: "composerContact",
      type: "array",
      title: "Composer Contact",
      description: "Composer contact information",
      of: [{ type: "block" }],
    },
  ],
  preview: {
    // NOTE: Override the default page title in the editor
    prepare: () => ({ title: "About Us" }),
  },
};
