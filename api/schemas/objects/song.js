import { MdMusicNote } from "react-icons/md";

export default {
  name: "song",
  type: "object",
  title: "Song",
  icon: MdMusicNote,
  fields: [
    {
      name: "name",
      type: "string",
      title: "Name",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "composer",
      type: "reference",
      title: "Composer",
      to: [{ type: "composer" }],
      validation: (Rule) => Rule.required(),
    },
  ],
  preview: {
    select: {
      composerFirstName: "composer.firstName",
      composerLastName: "composer.lastName",
      name: "name",
    },
    prepare({ composerFirstName, composerLastName, name }) {
      return {
        title: name,
        subtitle: `${composerFirstName} ${composerLastName}`,
      };
    },
  },
};
