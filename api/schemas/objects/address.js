export default {
  name: "address",
  type: "object",
  title: "Address",
  fields: [
    {
      name: "street",
      type: "string",
      title: "Street Name",
    },
    {
      name: "city",
      type: "string",
      title: "City",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "state",
      type: "string",
      title: "State",
      options: {
        list: [{ title: "PA", value: "PA" }],
      },
      validation: (Rule) => Rule.required(),
    },
    {
      name: "country",
      type: "string",
      title: "Country",
      options: {
        list: [{ title: "United States", value: "unitedStates" }],
      },
      validation: (Rule) => Rule.required(),
    },
    {
      name: "zipCode",
      type: "string",
      title: "Zip Code",
    },
  ],
  // NOTE: Currently only supported for document types
  /*initialValue: {
    state: "PA",
    country: "unitedStates",
  },*/
};
