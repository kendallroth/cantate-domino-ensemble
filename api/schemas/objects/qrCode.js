import { MdCode } from "react-icons/md";

export default {
  name: "qrCode",
  type: "object",
  title: "QR Code",
  icon: MdCode,
  fields: [
    {
      name: "name",
      type: "string",
      title: "Name",
      description: "Displayed name/label",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "image",
      type: "image",
      title: "QR Code",
      description: "Scannable QR code image (300x300 PNG)",
      options: { hotspot: true },
      validation: (Rule) => Rule.required(),
    },
    {
      name: "type",
      type: "string",
      title: "Type",
      description: "Type of QR code",
      options: {
        list: [{ title: "Donation", value: "donation" }],
      },
      validation: (Rule) => Rule.required(),
    },
  ],
};
