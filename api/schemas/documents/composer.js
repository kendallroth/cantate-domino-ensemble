import { MdFace } from "react-icons/md";

// TODO: Add profile photos

export default {
  name: "composer",
  type: "document",
  title: "Composer",
  icon: MdFace,
  fields: [
    {
      name: "firstName",
      type: "string",
      title: "First Name",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "lastName",
      type: "string",
      title: "Last Name",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "email",
      type: "string",
      title: "Email",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "address",
      type: "string",
      title: "Address",
      description: "Short address (city, province)",
    },
    {
      name: "startDate",
      type: "date",
      title: "Start Date",
      description: "When composer started writing music",
    },
    {
      name: "socialMedia",
      type: "array",
      description: "Social media links",
      of: [{ type: "socialMedia" }],
    },
    {
      name: "photo",
      type: "image",
      title: "Photo",
      options: { hotspot: true },
    },
    {
      name: "bio",
      type: "text",
      title: "Biography",
      description: "Short biography about the composer",
      rows: 3,
    },
  ],
  preview: {
    select: {
      firstName: "firstName",
      lastName: "lastName",
    },
    prepare({ firstName = "", lastName = "" }) {
      return {
        title: `${firstName} ${lastName}`,
      };
    },
  },
  orderings: [
    {
      title: "Name",
      name: "name",
      by: [
        { field: "lastName", direction: "asc" },
        { field: "firstName", direction: "asc" },
      ],
    },
  ],
};
