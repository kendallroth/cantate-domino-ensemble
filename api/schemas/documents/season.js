import { isAfter, parseISO } from "date-fns";
import { MdDateRange } from "react-icons/md";

export default {
  name: "season",
  type: "document",
  title: "Season",
  icon: MdDateRange,
  fieldsets: [
    {
      name: "publishing",
      title: "Publishing",
    },
  ],
  fields: [
    {
      name: "name",
      type: "string",
      title: "Name",
      description: "Short description of the season (ie. '2019 Fall').",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "slug",
      type: "slug",
      title: "URL Slug",
      description:
        "Slug used for the season page URL (DO NOT CHANGE FREQUENTLY!)",
      options: {
        source: "name",
      },
      validation: (Rule) => Rule.required(),
    },
    {
      name: "startDate",
      type: "date",
      title: "Start Date",
      description: "End of season (including practices)",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "endDate",
      type: "date",
      title: "End Date",
      description: "End of season (including scheduled programs)",
      validation: (Rule) =>
        Rule.required().custom(
          (value, context) =>
            isAfter(
              parseISO(context.document.endDate),
              parseISO(context.document.startDate)
            ) || "End date must be after start date"
        ),
    },
    {
      name: "theme",
      type: "text",
      title: "Theme",
      description: "Theme of the season music",
      rows: 3,
    },
    {
      name: "revealMembers",
      type: "boolean",
      title: "Reveal Members",
      description: "Whether members are revealed in website",
      initialValue: false,
      fieldset: "publishing",
    },
    {
      name: "revealComposers",
      type: "boolean",
      title: "Reveal Composers",
      description: "Whether composers are revealed in website",
      initialValue: false,
      fieldset: "publishing",
    },
    {
      name: "revealPrograms",
      type: "boolean",
      title: "Reveal Programs",
      description: "Whether programs are revealed in website",
      initialValue: false,
      fieldset: "publishing",
    },
    {
      name: "revealSongs",
      type: "boolean",
      title: "Reveal Songs",
      description: "Whether songs are revealed in website",
      initialValue: false,
      fieldset: "publishing",
    },
    {
      name: "members",
      type: "array",
      title: "Members",
      validation: (Rule) =>
        Rule.unique().error("Member can only be added to a season once"),
      of: [{ type: "seasonMember" }],
    },
    {
      name: "songs",
      type: "array",
      title: "Songs",
      of: [{ type: "song" }],
    },
    {
      name: "programs",
      type: "array",
      title: "Programs",
      of: [{ type: "program" }],
    },
  ],
  orderings: [
    {
      title: "Season Date",
      name: "seasonDate",
      by: [{ field: "startDate", direction: "desc" }],
    },
  ],
};
