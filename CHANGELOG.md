# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.3.0] - 2021-12-04
### Added
- Enable hiding season members, composers, songs, and programs
- QR codes for donations

### Changed
- Updated site logo
- Sort season member list

## [1.2.0] - 2021-04-12
### Changed
- Modified website favicon, theme, font

## [1.1.0] - 2021-04-01
### Added
- Bandzoogle store link in header

### Changed
- Modified website theme

## [1.0.0] - 2021-01-26
### Added
- Site layout and structure (header, footer, content)
- Initial landing page
- "About Us" page with minor content, composer contact, and member list
- "Members" page with member bios
- "Seasons" page and "Season Details" pages
- "Composers" page with composer information
